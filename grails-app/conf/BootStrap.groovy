import com.example.State
import com.example.Country
// source: http://jermdemo.blogspot.com/2009/05/loading-files-in-grails-bootstrap.html
import org.codehaus.groovy.grails.commons.ApplicationHolder

class BootStrap {

  def init = { servletContext ->
    // The grails csv plugin is required: http://grails.org/plugin/csv
    // Place the csv file in ./grails-app/conf/resources - you may have to create the directory
    def filePath = "resources/stvstat.csv"

    // This line lets the application know where the file is located:
    def appHolder = ApplicationHolder.application.parentContext.getResource("classpath:$filePath")

    // This line uses the csv grails plugin to open an inputStream and parse
    // eachCsvLine into the mapped domain field names:
    appHolder.inputStream.eachCsvLine { tokens ->
      new State(sortOrder:tokens[0].trim(),
        code:tokens[1].trim(),
        fullName:tokens[2].trim(),
        ediEquiv:tokens[3].trim(),
        updatedByEmail:"deinerso@unm.edu").save()
    }

    // Load grails-app/conf/resources/country.csv
    filePath = "resources/country.csv"

    // This line lets the application know where the file is located:
    appHolder = ApplicationHolder.application.parentContext.getResource("classpath:$filePath")

    // This line uses the csv grails plugin to open an inputStream and parse
    // eachCsvLine into the mapped domain field names:
    appHolder.inputStream.eachCsvLine { tokens ->
      new Country(sortOrder:tokens[0].trim(),
        commonName:tokens[1].trim(),
        formalName:tokens[2].trim(),
        type:tokens[3].trim(),
        subType:tokens[4].trim(),
        sovereignty:tokens[5].trim(),
        capital:tokens[6].trim(),
        iso4217CurrencyCode:tokens[7].trim(),
        iso4217CurrencyName:tokens[8].trim(),
        itu_tTelephoneCode:tokens[9].trim(),
        iso3166_1_2LetterCode:tokens[10].trim(),
        iso3166_1_3LetterCode:tokens[11].trim(),
        iso3166_1_Number:tokens[12].trim(),
        ianaCountryCodeTld:tokens[13].trim(),
        updatedByEmail:"deinerso@unm.edu"
      ).save()
    }
  }

  def destroy = {
    
  }
}
