package com.example

class Country {

  int sortOrder
  String commonName
  String formalName
  String type
  String subType
  String sovereignty
  String capital
  String iso4217CurrencyCode
  String iso4217CurrencyName
  String itu_tTelephoneCode
  String iso3166_1_2LetterCode
  String iso3166_1_3LetterCode
  String iso3166_1_Number
  String ianaCountryCodeTld
  String updatedByEmail
  Date dateCreated
  Date lastUpdated

  static constraints = {
    commonName(blank:false, nullable:false, unique:true)
    formalName(blank:true, nullable:true, maxSize:90)
    capital(blank:true, nullable:true, maxSize:90)
    iso4217CurrencyCode(blank:true, nullable:true)
    iso4217CurrencyName(blank:true, nullable:true)
    itu_tTelephoneCode(blank:true, nullable:true)
    iso3166_1_2LetterCode(blank:true, nullable:true, maxSize:2)
    iso3166_1_3LetterCode(blank:true, nullable:true, maxSize:3)
    iso3166_1_Number(blank:true, nullable:true)
    ianaCountryCodeTld(blank:true, nullable:true, maxSize:30)
    type(blank:true, nullable:true, maxSize:90)
    subType(blank:true, nullable:true, maxSize:90)
    sovereignty(blank:true, nullable:true, maxSize:90)
    sortOrder(min:1, unique:true)
    updatedByEmail(email:true, blank:false, nullable:false, maxSize:254, editable:false)
    dateCreated(editable:false)
    lastUpdated(editable:false)
  }

  static mapping = {
    sort "sortOrder"
  }

  String toString(){
    return "${commonName}"
  }

  def beforeInsert() {
    // TODO make this an automagic field/column once the app is aware of the current user:
//    updatedByEmail = ""
  }

  def beforeUpdate() {
    // TODO make this an automagic field/column once the app is aware of the current user:
//    updatedByEmail = ""
  }
}
