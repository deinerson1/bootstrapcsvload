package com.example

class State {

  String code
  String fullName
  String ediEquiv
  Integer sortOrder
  String updatedByEmail
  Date dateCreated
  Date lastUpdated


  static constraints = {
    code(blank: false, nullable: false, size: 2..2, unique: true)
    fullName(blank: true, nullable: true, size: 1..90)
    ediEquiv(blank: true, nullable: true, size: 2..2, maxSize: 2)
    sortOrder(blank: false, nullable: false, display:true)
    updatedByEmail(email:true, blank: true, nullable: true, size: 8..254, editable:false)
    dateCreated(editable:false)
    lastUpdated(editable:false)
  }

  static mapping = {
    sort "sortOrder"
  }

  String toString(){
    return "${fullName}"
  }

  def beforeInsert() {
    // TODO make this an automagic field/column once the app is aware of the current user:
//    updatedByEmail = User.current
  }

  def beforeUpdate() {
    // TODO make this an automagic field/column once the app is aware of the current user:
//    updatedByEmail = User.current
  }

  // This event auto-trims the columns named that aren't null:
  def beforeValidate() {
    code = code?.trim()
    fullName = fullName?.trim()
    ediEquiv = ediEquiv?.trim()
    updatedByEmail = updatedByEmail?.trim()
   }
}
